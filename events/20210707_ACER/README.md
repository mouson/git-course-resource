## 問題：在從 master rebase develop 且過程發生衝突，在解衝突後，將變更新增到 stage 後，於 Sourcetree 選擇 commit 無法持續完成 rebase 動作。

### 1. 平台 Windows 上的 Sourcetree (3.4.5)

主要問題在於發生衝突並解決衝突後，必須要透過 Sourcetree 的介面上 ** Actions -> Continue Rebase ** 來繼續完成 Rebase 的動作。如下圖：

![sourcetree -> actions -> continue rebase](./images/08GitRebaseConflict_windows_02.jpg)

完成操作過程如下 gif 動畫：

![sourcetree gif](./images/08GitRebaseConflict_windows_01.gif)

### 2. 平台 Windows 使用 PowerShell rebase 解衝突過程

![sourcetree -> actions -> continue rebase](./images/08GitRebaseConflict_PowerShell_03.jpg)

### 3. 平台 macOS 使用 PowerShell rebase 並解衝突之過程

- 與 Windows 不一樣的地方是，在解好衝突後，在 macOS 的 Sourcetree 介面上，可以直接點選 commit，會跳出讓使用者選擇 Continue Rebase 的選項供選擇。

![sourcetree -> commit -> continue rebase](./images/08GitRebaseConflict_macOS_04.gif)
