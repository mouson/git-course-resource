# Git Course Resource

## 一、Git 入門觀念到實作

* 範例下載：

透過 [Git Course Resource](https://gitlab.com/mouson/git-course-resource) 這個專案，進入 Repository 選擇 Tags 可以看到目前的版本，可下載 Example.zip

![Download Example File](./images/DownloadExample_02.jpg)

* 設定 comder 的 git 環境，不管換行符號：

```sh
git config --global core.autocrlf false
```

* 確認 autocrlf 設定為 false：

```sh
git config --get core.autocrlf
```

![Git Core Autocrlf False](./images/git_core_autocrlf_false.png)

* 將範例載入 SourceTree 可以用拖拉的方式：

![Project Can drop to SourceTree](./images/ExampleImportToSourceTree.gif)

### commit ref

1. 將標題與內容中間多一行空白

2. 標題限制 50 字元
3. 標題第一個字必須為大寫
4. 標題最後不要帶上句號
5. 標題內容可以使用強烈的語氣
6. 內容請用 72 字元來斷行
7. 內容可以解釋 what and why vs. how

### REF:

1. [How to Write a Git Commit Message](https://chris.beams.io/posts/git-commit/) - https://chris.beams.io/posts/git-commit/
2. [Conventional Commits](https://www.conventionalcommits.org/) - https://www.conventionalcommits.org/

## 二、Git 流程管理

1. [A successful Git branching model](https://nvie.com/posts/a-successful-git-branching-model/) - https://nvie.com/posts/a-successful-git-branching-model/
2. [Understanding the GitHub flow](https://guides.github.com/introduction/flow/) - https://guides.github.com/introduction/flow/
3. [GitLab Flow](https://about.gitlab.com/2014/09/29/gitlab-flow/) - https://about.gitlab.com/2014/09/29/gitlab-flow/
4. [The 11 Rules of GitLab Flow](https://about.gitlab.com/2016/07/27/the-11-rules-of-gitlab-flow/) - https://about.gitlab.com/2016/07/27/the-11-rules-of-gitlab-flow/
